# 08-Repair-HoleFill
include_directories(${PROJECT_SOURCE_DIR})

# OpenMesh
set(OPENMESH_PATH ${PROJECT_SOURCE_DIR}/thirdparty/OpenMesh-3.3)
include_directories(${OPENMESH_PATH}/include)
link_directories(${OPENMESH_PATH}/lib)

# freeglut
set(FREEGLUT_PATH ${PROJECT_SOURCE_DIR}/thirdparty/freeglut)
include_directories(${FREEGLUT_PATH}/include)
link_directories(${FREEGLUT_PATH}/lib/x64)

# Eigen
set(EIGEN_PATH ${PROJECT_SOURCE_DIR}/thirdparty/eigen-3.2.4)
include_directories(${EIGEN_PATH})

# Source
set(holl_fill_incs				HoleFiller.h
								OpenMeshUtils.h
)
set(holl_fill_srcs				holefill.cpp
								HoleFiller.cpp
)

SOURCE_GROUP("include" FILES ${holl_fill_incs})
SOURCE_GROUP("sources" FILES ${holl_fill_srcs})

add_definitions(-D_USE_MATH_DEFINES)
add_executable(08-Repair-HoleFill ${holl_fill_incs} ${holl_fill_srcs})
target_link_libraries(08-Repair-HoleFill 	debug OpenMeshCored OpenMeshToolsd optimized OpenMeshCore OpenMeshTools					# OpenMesh
											freeglut						 														# freeglut
)