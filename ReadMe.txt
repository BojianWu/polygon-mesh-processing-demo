Polygon Mesh Processing - Demo
==============================
The original code is downloaded from the official website: http://www.pmp-book.org. Thanks all the contributors.

If you want to learn some classical algorithms in polygon mesh processing, this book is right for you. Here are some pieces of codes, it may help you understand basic principles and formulations better in the book.

So, why I try to modify the original code?

Because in their implementation, all the related spare matrix decomposition and solving Laplacian equation are executed by TaucsSolver, which is an old but efficient library of sparse linear solvers. For some reason, I can not build this library successfully, so I replace all the corresponding code segments by Eigen, and it works well for me.

Besides, I reorganize whole code with CMake and include all the third party libraries.

As illustrated above, this repository includes all the files you need, containing the third party libraries such as freeglut, OpenMesh, Eigen etc. Before building the project, you should configure the environment variables correctly, for example, we have two third party libraries, you need to add corresponding path to the SYSTEM PATH like follows.

	<dir>/thirdparty/freeglut/bin/x64
	<dir>/thirdparty/OpenMesh-3.3/dll

Note that, the third party libraries are built on Visual Studio 2012, it is strongly recommended to use it as your coding environment, otherwise, you may need to re-compile the related libraries, I have not tested on other platforms.

If you have some problems, please feel free to contact me.

Thank you very much.